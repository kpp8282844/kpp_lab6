import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.io.Serial;

public class AdminRecord implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private Subscription reader;
    private Book borrowedBook;
    private Date borrowDate;
    private Date plannedReturnDate;
    private Date actualReturnDate;

    public AdminRecord() {
        reader = null;
        borrowedBook = null;
        borrowDate = null;
        plannedReturnDate = null;
        actualReturnDate = null;
    }

    public AdminRecord(Subscription reader, Book borrowedBook, Date borrowDate, Date plannedReturnDate) {
        this.reader = reader;
        this.borrowedBook = borrowedBook;
        this.borrowDate = borrowDate;
        this.plannedReturnDate = plannedReturnDate;
        this.actualReturnDate = null;
    }

    public Subscription getReader() {
        return reader;
    }

    public void setReader(Subscription reader) {
        this.reader = reader;
    }

    public Book getBorrowedBook() {
        return borrowedBook;
    }

    public void setBorrowedBook(Book borrowedBook) {
        this.borrowedBook = borrowedBook;
    }

    public Date getBorrowDate() {
        return borrowDate;
    }

    public void setBorrowDate(Date borrowDate) {
        this.borrowDate = borrowDate;
    }

    public Date getPlannedReturnDate() {
        return plannedReturnDate;
    }

    public void setPlannedReturnDate(Date plannedReturnDate) {
        this.plannedReturnDate = plannedReturnDate;
    }

    public Date getActualReturnDate() {
        return actualReturnDate;
    }

    public void setActualReturnDate(Date actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    public void recordBorrow(Subscription reader, Book borrowedBook, Date borrowDate, Date plannedReturnDate) {
        this.reader = reader;
        this.borrowedBook = borrowedBook;
        this.borrowDate = borrowDate;
        this.plannedReturnDate = plannedReturnDate;
        this.actualReturnDate = null;
    }

    public void recordReturn(Date actualReturnDate) {
        this.actualReturnDate = actualReturnDate;
    }

    @Override
    public String toString() {
        return "AdminRecord{" +
                "reader=" + reader +
                ", borrowedBook=" + borrowedBook +
                ", borrowDate=" + borrowDate +
                ", plannedReturnDate=" + plannedReturnDate +
                ", actualReturnDate=" + actualReturnDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdminRecord that = (AdminRecord) o;
        return Objects.equals(reader, that.reader) &&
                Objects.equals(borrowedBook, that.borrowedBook) &&
                Objects.equals(borrowDate, that.borrowDate) &&
                Objects.equals(plannedReturnDate, that.plannedReturnDate) &&
                Objects.equals(actualReturnDate, that.actualReturnDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(reader, borrowedBook, borrowDate, plannedReturnDate, actualReturnDate);
    }
}
