import java.io.Serializable;
import java.util.*;
import java.io.Serial;

public class Subscription implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    private String lastName;
    private String firstName;
    private String patronymic;
    private String emailAddress;
    private Set<Book> books;

    // Constructors
    public Subscription() {
        books = new HashSet<>();
    }

    public Subscription(String lastName, String firstName, String patronymic, String emailAddress, Set<Book> books) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.emailAddress = emailAddress;
        this.books = new HashSet<>();
        this.books.addAll(books);
    }

    public Subscription(String lastName, String firstName, String patronymic, String emailAddress) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.emailAddress = emailAddress;
        this.books = new HashSet<>();
    }

    // Getters and Setters
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    public void addBook(Book book) {
        this.books.add(book);
    }

    public void removeBook(Book book) {
        this.books.removeIf(book::equals);
    }

    // Override equals method
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Subscription that = (Subscription) o;
        return Objects.equals(lastName, that.lastName) &&
                Objects.equals(firstName, that.firstName) &&
                Objects.equals(patronymic, that.patronymic) &&
                Objects.equals(emailAddress, that.emailAddress) &&
                Objects.equals(books, that.books);
    }

    // Override hashCode method
    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, patronymic, emailAddress);
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", books=" + books +
                '}';
    }
}
