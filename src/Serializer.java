import java.io.*;

public class Serializer {

    // Method to serialize a Library object to a file
    public static void serializeLibrary(Library library, String filePath) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filePath))) {
            oos.writeObject(library);
            System.out.println("Library serialized successfully to: " + filePath);
        } catch (IOException e) {
            System.out.println("Error during serialization: " + e.getMessage());
            e.printStackTrace();
        }
    }

    // Method to deserialize a Library object from a file
    public static Library deserializeLibrary(String filePath) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filePath))) {
            Library library = (Library) ois.readObject();
            System.out.println("Library deserialized successfully from: " + filePath);
            return library;
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Error during deserialization: " + e.getMessage());
            e.printStackTrace();
            return null;
        }

    }
}
