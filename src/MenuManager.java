import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Scanner;

public abstract class MenuManager {

    public static void displayMenu(Library library) {
        Scanner scanner = new Scanner(System.in);
        int choice;
        do {
            System.out.println("Library Utils Menu");
            System.out.println("1. Sort Books by Year");
            System.out.println("2. Get Emails for Readers with 3+ Books");
            System.out.println("3. Get Borrowed Books of Author");
            System.out.println("4. Get Most Books Borrowed by a Reader");
            System.out.println("5. Notify Readers");
            System.out.println("6. Get Debtor List");
            System.out.println("0. Exit");
            System.out.print("Enter your choice: ");

            choice = scanner.nextInt();
            scanner.nextLine(); // Consume the newline character

            switch (choice) {
                case 1:
                    LibraryUtils.sortBooksByYear(library);
                    System.out.println("Books sorted by year.");
                    library.getBooks().forEach(System.out::println);
                    System.out.println("\n");
                    break;
                case 2:
                    Collection<String> emails = LibraryUtils.getEmailsForReadsWith3PlusBooks(library);
                    System.out.println("Emails for readers with 3+ books: " + emails);
                    break;
                case 3:
                    System.out.print("Enter author name: ");
                    String authorName = scanner.nextLine();
                    long numberOfBooks = LibraryUtils.getBorrowedBooksOfAuthor(library, authorName);
                    System.out.println("Number of borrowed books by " + authorName + ": " + numberOfBooks);
                    break;
                case 4:
                    long mostBooksBorrowed = LibraryUtils.getMostBooksBorrowedByReader(library);
                    System.out.println("Most books borrowed by a reader: " + mostBooksBorrowed);
                    break;
                case 5:
                    LibraryUtils.notifyReaders(library);
                    break;
                case 6:
                    getDebtorList(library);
                    break;
                case 0:
                    System.out.println("Exiting the Library Utils Menu. Goodbye!");
                    break;
                default:
                    System.out.println("Invalid choice. Please enter a valid option.");
            }
        } while (choice != 0);

        scanner.close();
    }

    private static void getDebtorList(Library library) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter current date (yyyy-MM-dd): ");
        String currentDateStr = scanner.nextLine();

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date currentDate = dateFormat.parse(currentDateStr);
            LibraryUtils.getDebtorList(library, currentDate);
        } catch (ParseException e) {
            System.out.println("Invalid date format. Please enter a date in the format yyyy-MM-dd.");
        }
    }
}
