import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class Main {
    public static void main(String[] args) throws ParseException {

        Library library = getLibrary();

        Serializer.serializeLibrary(library, "library.ser");

        Library deserializedLibrary = Serializer.deserializeLibrary("library.ser");

        if (deserializedLibrary != null) {
            MenuManager.displayMenu(library);
        }

    }

    private static Library getLibrary() throws ParseException {
        Library library = new Library();
        Subscription subscription1 = new Subscription("Last1", "First1", "Patronymic1", "email1@example.com");
        Subscription subscription2 = new Subscription("Last2", "First2", "Patronymic2", "email2@example.com");
        Subscription subscription3 = new Subscription("Last3", "First3", "Patronymic3", "email3@example.com");

        Book book1 = new Book("Author1", "Title1", 2021);
        Book book2 = new Book("Author2", "Title2", 2022);
        Book book3 = new Book("Author3", "Title3", 2020);

        library.addBook(book1);
        library.addBook(book2);
        library.addBook(book3);

        library.addSubscription(subscription1);
        library.addSubscription(subscription2);
        library.addSubscription(subscription3);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
   //     Date currentDate = dateFormat.parse("2023-01-01");
        Date plannedReturnDate1 = dateFormat.parse("2022-12-01");
        Date plannedReturnDate2 = dateFormat.parse("2022-11-15");

        library.borrowBook(subscription1, book1, new Date(), plannedReturnDate1);
        library.borrowBook(subscription1, book2, new Date(), plannedReturnDate1);
        library.borrowBook(subscription1, book3, new Date(), plannedReturnDate1);

        library.borrowBook(subscription2, book2, new Date(), plannedReturnDate2);

        return library;
    }
}