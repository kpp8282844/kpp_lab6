import java.io.Serializable;
import java.util.*;
import java.io.Serial;

public class Library implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Set<Book> books;
    private Set<Subscription> subscriptions;
    private List<AdminRecord> borrowingRecords;

    public Library() {
        this.books = new LinkedHashSet<>();
        this.subscriptions = new LinkedHashSet<>();
        this.borrowingRecords = new ArrayList<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void addBooks(Collection<Book> books){
        this.books.addAll(books);
    }

    public Set<Book> getBooks(){
        return books;
    }

    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void addSubscription(Subscription subscription) {
        subscriptions.add(subscription);
    }

    public void borrowBook(Subscription reader, Book book, Date borrowDate, Date plannedReturnDate) {
        if (books.contains(book) && subscriptions.contains(reader)) {
            AdminRecord adminRecord = new AdminRecord();
            adminRecord.recordBorrow(reader, book, borrowDate, plannedReturnDate);
            borrowingRecords.add(adminRecord);
            reader.addBook(book);
        } else {
            System.out.println("Book or subscription not found in the library.");
        }
    }

    public void returnBook(Book book, Date actualReturnDate) {
        for (AdminRecord adminRecord : borrowingRecords) {
            if (adminRecord.getBorrowedBook().equals(book) && adminRecord.getActualReturnDate() == null) {
                adminRecord.recordReturn(actualReturnDate);
                break;  // Assuming a book can only be borrowed once in this example
            }
        }
    }

    public List<AdminRecord> getBorrowingRecords() {
        return borrowingRecords;
    }
}
