import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

public abstract class LibraryUtils {

    public static void sortBooksByYear(Library library) {
        Set<Book> books = library.getBooks();

        Set<Book> sortedBooks = books.stream()
                .sorted(Comparator.comparingInt(Book::getYearOfPublication))
                .collect(Collectors.toCollection(LinkedHashSet::new));

        books.clear();
        books.addAll(sortedBooks);
    }

    public static Collection<String> getEmailsForReadsWith3PlusBooks(Library library){
        Set<String> emails;

        var subscriptions = library.getSubscriptions();

        emails = subscriptions.stream().filter((s) -> s.getBooks().size() > 2).map(Subscription::getEmailAddress).collect(Collectors.toSet());

        return emails;
    }

    public static long getBorrowedBooksOfAuthor(Library library, String authorName){
        long numberOfBooks = 0;

        var adminRecords = library.getBorrowingRecords();

        numberOfBooks = adminRecords.stream()
                .filter((r) -> Objects.equals(r.getBorrowedBook()
                        .getAuthor(), authorName)).map(AdminRecord::getReader).distinct().count();

        return numberOfBooks;
    }

    public static long getMostBooksBorrowedByReader(Library library) {
        long numberOfBooks = 0;

        var subscriptions = library.getSubscriptions();

        var booksArray = subscriptions.stream().map((s) -> s.getBooks()).max(Comparator.comparingInt(Set::size)).get();
        numberOfBooks = booksArray.size();
        return numberOfBooks;
    }

    public static void notifyReaders(Library library) {
        var subscribers = library.getSubscriptions();

        Map<Boolean, List<Subscription>> readersByBookNumber =
                subscribers.stream().collect(
                        Collectors.partitioningBy(s -> s.getBooks().size() < 2));

         String notifyStringNewBooks = "Dear readers! Check our new exciting books!";
         String notifyStringReturnBooks = "Dear readers! Remember to return borrowed books on planned time!";

        List<Subscription> newBooksReaders = new ArrayList<>();
        List<Subscription> returnBooksReaders = new ArrayList<>();

        readersByBookNumber.forEach((k, v) -> {
            if(k)
                newBooksReaders.addAll(v);
            else
                returnBooksReaders.addAll(v);
        });

        List<String> lowReaders;
        List<String> highReaders;

        lowReaders = newBooksReaders.stream().map((s) ->
                        s.getLastName() + " " + s.getFirstName() + " " + s.getPatronymic())
                .collect(Collectors.toList());
        highReaders = returnBooksReaders.stream().map((s) ->
                        s.getLastName() + " " + s.getFirstName() + " " + s.getPatronymic())
                .collect(Collectors.toList());

        if (!lowReaders.isEmpty()) {
            System.out.println(notifyStringNewBooks);
            lowReaders.forEach(System.out::println);
            System.out.println("\n");
        }

        if (!highReaders.isEmpty()) {
            System.out.println(notifyStringReturnBooks);
            highReaders.forEach(System.out::println);
        }
    }

    public static void getDebtorList(Library library, Date currentDate) {
        List<AdminRecord> debtors = library.getBorrowingRecords().stream()
                .filter((r) -> r.getActualReturnDate() == null)
                .sorted(Comparator.comparing((AdminRecord r) -> r.getReader().getLastName())
                        .thenComparing(r -> r.getReader().getFirstName()))
                .toList();

        debtors.forEach(adminRecord -> {
            Subscription reader = adminRecord.getReader();
            Book borrowedBook = adminRecord.getBorrowedBook();
            Date plannedReturnDate = adminRecord.getPlannedReturnDate();

            // Calculate days passed since planned return date
            long daysPassed = Duration.between(plannedReturnDate.toInstant(), currentDate.toInstant()).toDays();

            // Print debtor information
            System.out.println("Debtor: " + reader.getFirstName() + " " + reader.getLastName());
            System.out.println("Borrowed Book: " + borrowedBook.getTitle());
            System.out.println("Days Passed Since Planned Return: " + daysPassed + " days");
            System.out.println("----");
        });
    }

}
