import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class LibraryUtilsTest {

    @Test
    public void testBooksSorting(){
        Set<Book> bookSet = new LinkedHashSet<>();

        Book book1 = new Book("Author1", "Title1", 2021);
        Book book2 = new Book("Author2", "Title2", 2022);
        Book book3 = new Book("Author3", "Title3", 2020);
        Book book4 = new Book("Author4", "Title4", 2023);

        bookSet.add(book1);
        bookSet.add(book2);
        bookSet.add(book3);
        bookSet.add(book4);

        Set<Book> bookSetSorted = new LinkedHashSet<>();
        bookSetSorted.add(book3);
        bookSetSorted.add(book1);
        bookSetSorted.add(book2);
        bookSetSorted.add(book4);

        Library library = new Library();
        library.addBooks(bookSet);
        LibraryUtils.sortBooksByYear(library);

        assertIterableEquals(library.getBooks(), bookSetSorted, "Books are not in the expected order");
    }

    @Test
    void testGetEmailsForReadsWith3PlusBooks() {
        Library library = new Library();

        Subscription subscription1 = new Subscription("Last1", "First1", "Patronymic1", "email1@example.com");
        Subscription subscription2 = new Subscription("Last2", "First2", "Patronymic2", "email2@example.com");
        Subscription subscription3 = new Subscription("Last3", "First3", "Patronymic3", "email3@example.com");

        Book book1 = new Book("Author1", "Title1", 2021);
        Book book2 = new Book("Author2", "Title2", 2022);
        Book book3 = new Book("Author3", "Title3", 2020);

        subscription1.addBook(book1);
        subscription1.addBook(book2);
        subscription1.addBook(book3);

        subscription2.addBook(book1);
        subscription2.addBook(book2);

        subscription3.addBook(book1);

        library.addSubscription(subscription1);
        library.addSubscription(subscription2);
        library.addSubscription(subscription3);

        Collection<String> resultEmails = LibraryUtils.getEmailsForReadsWith3PlusBooks(library);

        Set<String> expectedEmails = new HashSet<>(List.of("email1@example.com"));
        assertEquals(expectedEmails, new HashSet<>(resultEmails), "Incorrect emails for readers with 3 or more books");
    }

    @Test
    void testGetBorrowedBooksOfAuthor() {
        Library library = new Library();

        Book book1 = new Book("Author1", "Title1", 2021);

        Subscription subscription = new Subscription("Last1", "First1", "Patronymic1", "email1@example.com");
        library.addSubscription(subscription);
        library.addBook(book1);
        library.borrowBook(subscription, book1, new Date(), new Date());

        var result = LibraryUtils.getBorrowedBooksOfAuthor(library, "Author1");

        assertEquals(1, result, "Incorrect number of borrowed books by the specified author");
    }

    @Test
    void testGetMostBooksBorrowedByReader() {
        // Arrange
        Library library = new Library();

        // Create subscriptions with different numbers of borrowed books
        Subscription subscription1 = new Subscription("Last1", "First1", "Patronymic1", "email1@example.com");
        Subscription subscription2 = new Subscription("Last2", "First2", "Patronymic2", "email2@example.com");
        Subscription subscription3 = new Subscription("Last3", "First3", "Patronymic3", "email3@example.com");

        Book book1 = new Book("Author1", "Title1", 2021);
        Book book2 = new Book("Author2", "Title2", 2022);
        Book book3 = new Book("Author3", "Title3", 2020);

        // Adding books to subscriptions
        subscription1.addBook(book1);
        subscription1.addBook(book2);
        subscription1.addBook(book3);

        subscription2.addBook(book1);
        subscription2.addBook(book2);


        // Adding subscriptions to the library
        library.addSubscription(subscription1);
       library.addSubscription(subscription2);
       library.addSubscription(subscription3);

        // Act
        long result = LibraryUtils.getMostBooksBorrowedByReader(library);

        // Assert
        assertEquals(3, result, "Incorrect number of books borrowed by the reader with the most books");
    }

    @Test
    void testNotifyReaders() {
        Library library = new Library();

        Subscription lowReader1 = new Subscription("Roman", "Reader1", "Patronymic1", "email1@example.com");
        Subscription lowReader2 = new Subscription("Petro", "Reader2", "Patronymic2", "email2@example.com");
        Subscription highReader1 = new Subscription("Bu", "Reader1", "Patronymic1", "email3@example.com");

        Book book1 = new Book("Author1", "Title1", 2021);
        Book book2 = new Book("Author2", "Title2", 2022);
        Book book3 = new Book("Author3", "Title3", 2002);

        lowReader1.addBook(book1);
        lowReader2.addBook(book1);

        highReader1.addBook(book1);
        highReader1.addBook(book2);
        highReader1.addBook(book3);

        library.addSubscription(lowReader1);
        library.addSubscription(lowReader2);
        library.addSubscription(highReader1);

        assertDoesNotThrow(() -> LibraryUtils.notifyReaders(library));

    }

    @Test
    void testGetDebtorList() throws ParseException {
        // Arrange
        Library library = new Library();

        Subscription debtor1 = new Subscription("Debtor", "One", "Patronymic1", "email1@example.com");
        Subscription debtor2 = new Subscription("Debtor", "Two", "Patronymic2", "email2@example.com");

        Book book1 = new Book("Author1", "Title1", 2021);
        Book book2 = new Book("Author2", "Title2", 2022);

        library.addBook(book1);
        library.addBook(book2);

        debtor1.addBook(book1);
        debtor2.addBook(book2);

        library.addSubscription(debtor1);
        library.addSubscription(debtor2);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = dateFormat.parse("2023-01-01");
        Date plannedReturnDate1 = dateFormat.parse("2023-12-01");
        Date plannedReturnDate2 = dateFormat.parse("2022-11-15");

        library.borrowBook(debtor1, book1, new Date(), plannedReturnDate1);
        library.borrowBook(debtor2, book2, new Date(), plannedReturnDate2);

        library.returnBook(book1, new Date());

        LibraryUtils.getDebtorList(library, currentDate);
    }
}